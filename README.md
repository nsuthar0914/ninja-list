# Description
This is an alternative implementation for user profiles on tretton37 website.
We will be using ReactJS for the clientside implementation, testing-library for unit-testing, plain css for styling and cypress for e2e testing.

# User stories (10 points)
- No UI framework used (such as Bootstrap, Ant) - 1
- Responsive design, works on mobile and tablets - 2
- Sort by name and office - 1
- Filter by name and office - 1
- Filter by contact links - 1
- Unit tests for existing functionality (reasonable coverage) - 2
- End-to-end testing (with an existing framework) - 2

# Instructions

## Client
### Installation
- `cd client`
- `yarn`
### Development
- `yarn test`
- `yarn start`
### Release
- `yarn build`

## E2E Testing (Requires a development/test server of client running)
### Installation
- `cd e2e_tests`
- `yarn`
### Development
- `yarn cy`
### Test
- `yarn test:e2e`