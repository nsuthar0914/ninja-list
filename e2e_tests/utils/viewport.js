/**
 * Cypress viewport presets and custom viewport sizes
 *
 * Usage:
 *  - `cy.viewport(MOBILE)` in tests will set the viewport to `iphone-6` preset.
 *  - `cy.viewport(DESKTOP)` in tests will set the viewport to `macbook-11` preset.
 *  - `cy.viewport(someViewport.width, someViewport.height)` for custom dimensions
 *
 * Note: Add more relevant preset if required and use it in tests.
 * See here for more presets https://docs.cypress.io/api/commands/viewport.html#Arguments
 */

/** Basing desktop viewport to mackbook-11 which is 1366 X 768 */
export const DESKTOP = "macbook-11";

/** Basing mobile viewport to iphone-6 which is 375 X 667 */
export const MOBILE = "iphone-6";

/** Custom */
/* export const someViewport = {
    width: 1000,
    height: 500
} */
