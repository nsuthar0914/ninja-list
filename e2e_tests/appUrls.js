/**
 * URLs for using in tests.
 * NOTE:
 *  - URL keys listed here will have corresponding values set
 *    in `cypress.env.json` for local dev and `CYPRESS_<URL_KEY>` for CI
 *  - To add new URL, add the key to this list and set value
 *    in `cypress.env.json` for local dev and `CYPRESS_<URL_KEY>` for CI
 */
const ENV_URL_CONSTANTS = [
    "NINJA_URL",
];

/** Fetch the env config based on priority.
 *  - Dev: `cypress.env.json` values are fetched
 *  - CI:  any env variable that starts with `CYPRESS_` in `process.env` are fetched
 *
 *  NOTE: `process.env.CYPRESS_*` overwrites `cypress.env.json` if both exists
 *
 *  See this link below for the order of fetching the env values.
 *  https://docs.cypress.io/guides/guides/environment-variables.html#Setting
 */
const envConfig = Cypress.env();

/**
 * `ENV_URL_CONSTANTS` with values will be available in `URLs`
 *
 * Usage:
 * `import appURLs from '<path_to_this_file>'`;
 */
const URLs = Cypress._.pick(envConfig, ENV_URL_CONSTANTS);

export default URLs;
