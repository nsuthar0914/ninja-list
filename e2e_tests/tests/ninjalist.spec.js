import appUrls from "../appUrls";
import {DESKTOP, MOBILE} from "../utils/viewport";

describe("tretton37 ninja list", () => {
    const viewports = [{name: "Desktop", preset: DESKTOP}, {name: "Mobile", preset: MOBILE}];
    viewports.forEach((viewport, index) => {
        context(viewport.name, () => {
            beforeEach(() => {
                cy.viewport(viewport.preset);
                // Start the cypress server and define routes we need to wait for
                cy.server();
                cy.route("GET", "https://api.tretton37.com/ninjas").as("getNinjas");
            });

            it("Check Sorting", () => {
                cy.visit(appUrls.NINJA_URL);

                cy.wait("@getNinjas")
                    .its("status")
                    .should("eq", 200);

                cy.get(".cardInfoName").then($el => {
                    expect($el[0].innerText).to.eql("Adrian Sofinet");
                    expect($el[$el.length - 1].innerText).to.eql("Žiga Vajdič");

                    cy.get("[data-cy=sort-select]")
                        .scrollIntoView()
                        .select("name-descending");

                    cy.get(".cardInfoName").then($el => {
                        expect($el[$el.length - 1].innerText).to.eql("Adrian Sofinet");
                        expect($el[0].innerText).to.eql("Žiga Vajdič");
                    });
                });
            });
            it("Check Filter by contact", () => {
                cy.visit(appUrls.NINJA_URL);

                cy.wait("@getNinjas")
                    .its("status")
                    .should("eq", 200);

                cy.get(".cardInfoName").then($el => {
                    const total = $el.length;

                    cy.get("[data-cy=contact-select]")
                        .scrollIntoView()
                        .select("gitHub");

                    cy.get("[data-cy=github-link]").then($gl => {
                        expect($gl.length).not.to.eql(total);
                    });
                });
            });
            it("Check Filter by Text", () => {
                cy.visit(appUrls.NINJA_URL);

                cy.wait("@getNinjas")
                    .its("status")
                    .should("eq", 200);
                
                cy.get(".cardInfoName").then($el => {
                    const total = $el.length;

                    cy.findByPlaceholderText("Filter by name or office")
                        .type("ste");

                    cy.get(".cardInfoName").then($el1 => {
                        expect($el1.length).not.to.eql(total);
                    });
                });
            });
        });
    });
});
