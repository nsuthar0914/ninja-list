import React from 'react';
import './App.css';
import CardsList from './components/CardsList';
import Filters from './components/Filters';
import FiltersProvider from './providers/FiltersProvider';

function App() {
  return (
    <FiltersProvider>
        <div className="container">
          <h3>The fellowship of the tretton37</h3>
          <Filters />
          <CardsList />
        </div>
    </FiltersProvider>
  );
}

export default App;
