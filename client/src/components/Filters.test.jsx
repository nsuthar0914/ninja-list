import React, { useContext } from "react";
import {screen, render, wait, fireEvent} from "@testing-library/react";
import {LocalStorageMock} from "../hooks/useLocalStorage.test";
import FiltersProvider from "../providers/FiltersProvider";
import Filters from "./Filters";

let _localstorage;
describe("FiltersProvider", () => {
    beforeAll(() => {
        _localstorage = window.localStorage;
    });

    afterAll(() => {
        window.localStorage = _localstorage;
    });

    it("Initialstate and update", async () => {
        const {getByText, getByPlaceholderText, debug} = render(<FiltersProvider>
            <Filters />
        </FiltersProvider>);
        expect(getByPlaceholderText("Filter by name or office")).toBeTruthy();
        expect(screen.getByDisplayValue("Filter by contact link")).toBeTruthy();
        expect(screen.getByDisplayValue("Name Ascending")).toBeTruthy();
        fireEvent.change(screen.getByDisplayValue("Name Ascending"), {target: {value: "name-descending"}});
        await wait(() => {
            expect(screen.getByDisplayValue("Name Descending")).toBeTruthy();
        });
        fireEvent.change(screen.getByDisplayValue("Filter by contact link"), {target: {value: "gitHub"}});
        await wait(() => {
            expect(screen.getByDisplayValue("Github")).toBeTruthy();
        });
        fireEvent.change(getByPlaceholderText("Filter by name or office"), {target: {value: "ste"}});
        await wait(() => {
            expect(getByPlaceholderText("Filter by name or office").value).toBe("ste");
        });
        expect(await window.localStorage.getItem("sort")).toBe("\"name-descending\"");
        expect(await window.localStorage.getItem("textFilter")).toBe("\"ste\"");
        expect(await window.localStorage.getItem("contactFilter")).toBe("\"gitHub\"");
    });
});
