import React from "react";
import {render} from "@testing-library/react";
import Card from "./Card";

const cardData = {
    "name": "Stefan Viberg",
    "email": "fgrsna.ivoret@gerggba82.pbz",
    "phoneNumber": "+22288627219",
    "office": "Lund",
    "tagLine": null,
    "mainText": "\u003cp\u003eDuring my time as consultant, I have worked on projects at both large and small companies, which has given me a lot of useful experience. I want to expand my areas of knowledge whenever possible and keep up with new things in the industry.\u003c/p\u003e\u003cp\u003eI like sharing knowledge with others and I like to work in agile teams. My goal is to accomplish high quality .NET solutions delightfully and effectively using good architecture.\u003c/p\u003e\u003cp\u003eIn my spare time, I like to spend time with my wife and daughter. I also like hiking in the Swedish mountains.\u003c/p\u003e ",
    "gitHub": "stefanviberg",
    "twitter": "stefanviberg",
    "stackOverflow": null,
    "linkedIn": "/in/stefanviberg",
    "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/stefan-viberg",
    "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/stefan-viberg",
    "imageWallOfLeetUrl": null,
    "highlighted": false
};

describe("Card", () => {
    it("Snapshot", () => {
        const {container} = render(<Card {...cardData} />);
        expect(container).toMatchSnapshot();
    });
});