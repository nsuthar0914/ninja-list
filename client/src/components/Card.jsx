import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedinIn, faGithub, faTwitter } from '@fortawesome/free-brands-svg-icons'
import "./Card.css";

/**
 * This is a dumb component to display profile details for each ninja.
 * All the profile details made available by the api are being passed currently.
 * This could be changed later to pass only desired info alongwith splitting this into 
 * header, text and links composable units.
 * @param {*} props - profile details
 */
const Card = (props) => {
    return (
        <div className="card" key={props.email}>
            <div className="cardImage">
                <img src={props.imagePortraitUrl} />
            </div>
            <div className="cardInfo">
                <div className="cardInfoText">
                    <div className="cardInfoName">{props.name}</div>
                    <div className="cardInfoOffice">Office: {props.office}</div>
                </div>
                <div className="cardInfoLink">
                    {props.linkedIn
                        ? <div className="cardInfoLinkCircular">
                        <a href={`https://www.linkedin.com${props.linkedIn}`}>
                            <FontAwesomeIcon color="#ffffff" icon={faLinkedinIn} />
                        </a>
                    </div> : null}
                    {props.gitHub
                        ? <a href={`https://github.com/${props.gitHub}`}>
                        <FontAwesomeIcon icon={faGithub} data-cy="github-link" />
                    </a> : null}
                    {props.twitter
                        ? <div className="cardInfoLinkCircular">
                        <a href={`https://twitter.com/${props.twitter}`}>
                            <FontAwesomeIcon width="1px" color="#ffffff" icon={faTwitter} />  
                        </a>  
                    </div> : null}
                </div>
            </div>
        </div>
    );
};

export default Card;
