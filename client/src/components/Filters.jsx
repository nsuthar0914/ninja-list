import React from "react";
import { useContext } from "react";
import { FiltersContext } from "../providers/FiltersProvider";
import "./Filters.css";

/**
 * This is filters component that uses filters context to get and set various filters.
 * The various kinds of filters here could also be separated and tested separately. 
 */
const Filters = () => {
    const {
        sort,
        setSort,
        textFilter,
        setTextFilter,
        contactFilter,
        setContactFilter,
    } = useContext(FiltersContext);
    return (
        <div className="filtersContainer">
            <div className="filters">
                <input value={textFilter} onChange={e => setTextFilter(e.target.value)} placeholder="Filter by name or office" className="textinput"/>
            </div>
            <div className="filters">
                <select value={contactFilter} onChange={e => setContactFilter(e.target.value)} className="select" data-cy="contact-select">
                    <option value="">Filter by contact link</option>
                    <option value="gitHub">Github</option>
                    <option value="linkedIn">LinkedIn</option>
                    <option value="twitter">Twitter</option>
                </select>
            </div>
            <div className="sorting">
                Sort By:
                <select value={sort} onChange={e => setSort(e.target.value)} className="select" data-cy="sort-select">
                    <option value="name-ascending">Name Ascending</option>
                    <option value="name-descending">Name Descending</option>
                    <option value="office-ascending">Office Ascending</option>
                    <option value="office-descending">Office Descending</option>
                </select>
            </div>
        </div>
    );
};

export default Filters;
