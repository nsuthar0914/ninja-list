import React, { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { FiltersContext } from "../providers/FiltersProvider";
import Card from "./Card";
import "./CardsList.css";

const contactFilterFn = (profiles, contactFilter) => {
    return [...profiles].filter((profile) => {
        if (!contactFilter || profile[contactFilter]) {
            return true;
        }
        return false;
    });
};

const textFilterFn = (profiles, textFilter) => {
    const textFilterExp = new RegExp(textFilter, 'gi');
    return [...profiles].filter((profile) => {
        if (!textFilter || textFilterExp.test(profile.name) || textFilterExp.test(profile.office)) {
            return true;
        }
        return false;
    });
};

const sortFn = (profiles, sort) => {
    return [...profiles].sort((a, b) => {
        const [sortField, sortOrder] = sort.split("-");
        if(a[sortField] < b[sortField]) { return sortOrder === "ascending" ? -1 : 1; }
        if(a[sortField] > b[sortField]) { return sortOrder === "ascending" ? 1 : -1; }
        return 0;
    });
};

/**
 * This is a stateful component that deals with fetching, sorting and filtering profile details.
 * Major improvements required here  includes, separation of the filterfns and their testing,
 * separation of api config, error handling and loader component.
 */
const CardsList = () => {
    const [fetchedProfiles, setFetchedProfiles] = useState([]);
    const [profiles, setProfiles] = useState(fetchedProfiles);
    const [loading, setLoading] = useState(false);
    const {
        sort,
        textFilter,
        contactFilter,
    } = useContext(FiltersContext);
    useEffect(() => {
        setLoading(true);
        (async () => {
            try {
                const profileData = await fetch("https://api.tretton37.com/ninjas");
                let profileDataJson = await profileData.json();
                setFetchedProfiles(profileDataJson);
                if (contactFilter) {
                    profileDataJson = contactFilterFn(profileDataJson, contactFilter);
                }
                if (textFilter) {
                    profileDataJson = textFilterFn(profileDataJson, textFilter);
                }
                if (sort) {
                    profileDataJson = sortFn(profileDataJson, sort);
                }
                setProfiles(profileDataJson);
                setLoading(false);
            } catch (e) {
                console.log(e);
                setLoading(false);
            }
        })()
    }, []);
    useEffect(() => {
        let updatedProfiles = [...fetchedProfiles];
        if (contactFilter) {
            updatedProfiles = contactFilterFn(updatedProfiles, contactFilter);
        }
        if (textFilter) {
            updatedProfiles = textFilterFn(updatedProfiles, textFilter);
        }
        if (sort) {
            updatedProfiles = sortFn(updatedProfiles, sort);
        }
        setProfiles(updatedProfiles);
    }, [contactFilter, textFilter, sort]);
    if (loading) return <div>Loading ....</div>
    return (
        <>
            {profiles.length
                ?   <div className="cardsList">{profiles.map((profile) => <Card key={`${profile.email}`} {...profile} />)}</div>
                :   <div className="noResults">No profiles available currently</div>}
        </>
    );
};

export default CardsList;
