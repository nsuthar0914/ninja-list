import React from "react";
import {cleanup, render, wait} from "@testing-library/react";
import {rest} from 'msw'
import {setupServer} from 'msw/node'
import CardsList from "./CardsList";
import FiltersProvider from "../providers/FiltersProvider";
import {LocalStorageMock} from "../hooks/useLocalStorage.test";

const fetchedProfiles = [
    {
        "name": "Stefan Viberg",
        "email": "fgrsna.ivoret@gerggba82.pbz",
        "phoneNumber": "+22288627219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003eDuring my time as consultant, I have worked on projects at both large and small companies, which has given me a lot of useful experience. I want to expand my areas of knowledge whenever possible and keep up with new things in the industry.\u003c/p\u003e\u003cp\u003eI like sharing knowledge with others and I like to work in agile teams. My goal is to accomplish high quality .NET solutions delightfully and effectively using good architecture.\u003c/p\u003e\u003cp\u003eIn my spare time, I like to spend time with my wife and daughter. I also like hiking in the Swedish mountains.\u003c/p\u003e ",
        "gitHub": "stefanviberg",
        "twitter": "stefanviberg",
        "stackOverflow": null,
        "linkedIn": "/in/stefanviberg",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/stefan-viberg",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/stefan-viberg",
        "imageWallOfLeetUrl": null,
        "highlighted": false
    }, {
        "name": "Cajsa Nilsson",
        "email": "pnwfn.avyffba@gerggba82.pbz",
        "phoneNumber": "+28861977219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003eI enjoy creating cool and useful interfaces. I find UX exciting, and I love to play around with CSS. I have always had an interest in tech, and I want to deepen my knowledge by learning something new every day.\u003c/p\u003e\u003cp\u003eBesides work, I love to workout! My absolute favourite is HIIT, but I also do a lot of running, gym and yoga. Except for my passion for a great workout, I appreciate good food and play video games. I love to spend time with friends and family.\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": null,
        "linkedIn": "/in/cajsa-nilsson-a47a47150/",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/cajsa-nilsson-cv1",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/cajsa-nilsson-body",
        "imageWallOfLeetUrl": null,
        "highlighted": true
    }, {
        "name": "Joel Sannerstedt",
        "email": "wbry.fnaarefgrqg@gerggba82.pbz",
        "phoneNumber": "+28869807219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003eTo be presented\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": null,
        "linkedIn": "/in/joelsannerstedt/",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/joel-sannerstedt-cv2",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/joel-sannerstedt-body",
        "imageWallOfLeetUrl": null,
        "highlighted": false
    }, {
        "name": "Michał Łusiak",
        "email": "zvpuny.yhfvnx@gerggba82.pbz",
        "phoneNumber": "+69728861219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003eI wrote my first code when I was 7 and instantly fell in love with how empowering it feels. In my career I have completed many different types of programming, but have spent most time on web development. Outside of work I enjoy learning new things and programming phones or other small devices. I have also recently gotten into functional programming.\u003c/p\u003e\u003cp\u003eBesides coding, I like activities involving high speed and altitude. I love paragliding and falling out of planes! In general I’m interested in everything that flies and I’m planning to start flying sailplanes soon. A couple of years ago I also discovered that I really enjoy alpine skiing and I’m getting better at it every year!\u003c/p\u003e ",
        "gitHub": "mlusiak",
        "twitter": "mlusiak",
        "stackOverflow": "460822",
        "linkedIn": "/pub/michał-łusiak/a2/139/971",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/michal-lusiak",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/michal-lusiak",
        "imageWallOfLeetUrl": null,
        "highlighted": false
    }, {
        "name": "Nikki Sollid",
        "email": "avxxv.fbyyvq@gerggba82.pbz",
        "phoneNumber": "+63758581219",
        "office": "Stockholm",
        "tagLine": null,
        "mainText": "\u003cp\u003eI am a front end developer who believes in usability and simplifying your life as a user. If I can make your work more effective and give you a premium feel, whilst at the same time using my code and expertise then I’ve succeeded in my goal.\u003c/p\u003e\u003cp\u003eI started developing at 11 when my father first showed me different coding languages, and ever since that day I’ve been hooked.\u003c/p\u003e\u003cp\u003eI am in a constant state of improvement, always striving to learn new things. Knowledge is key, and as a developer you have life long homework ahead of you learning your craft.\u003c/p\u003e\u003cp\u003eI also love makeup, being sassy and mojitos.\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": "3987150",
        "linkedIn": "/in/nikki-sollid/",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/nikki-solid-cv",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/nikki-solid-body",
        "imageWallOfLeetUrl": null,
        "highlighted": true
    }, {
        "name": "Åsa Vukmanovic Lilja",
        "email": "nfn.yvywn@gerggba82.pbz",
        "phoneNumber": "+06528861219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003e​I have always worked with service in one way or another and I really value eye to eye meetings. I often throw myself into new challenges and think that \"All things are difficult before they are easy\" is a great saying. I have a thing for details....small things that put the icing on the cake. I would say that service is my super power and I am happy to perform my service skills here at tretton37.\u003c/p\u003e\u003cp\u003eMy family is of course my number 1 priority and travel is our passion. We are always planning that next trip. I am an enthusiastic sportsparent and I constantly chase my personal best at running a swedish mile. I like to be busy, surrounding myself with nice people and things to do. But I am also learning to enjoy the small things that brings you a happy life!\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": null,
        "linkedIn": "/in/åsa-vukmanovic-lilja-25b205a3",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/asa-lilja-cv",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/asa-lilja-body",
        "imageWallOfLeetUrl": null,
        "highlighted": true
    }, {
        "name": "Gustav Hallberg Hektor",
        "email": "thfgni.unyyoret@gerggba82.pbz",
        "phoneNumber": "+50288607219",
        "office": "Lund",
        "tagLine": null,
        "mainText": "\u003cp\u003eAs a young boy, I just couldn’t get enough of all things related to technology. I loved taking electronics apart and occasionally putting them back together again, and as a child I dreamed of becoming an electrician. At the end of the nineties we got a “home-pc” with an ADSL-connection, and my spare time was spent between either playing Counter Strike \u0026amp; MMORPGs with friends, or playing guitar and drums in a myriad of different school bands.\u003c/p\u003e\u003cp\u003eMy love of all things tech-related, together with my decent problem solving skills, meant that I took on the role as unofficial support technician - whether it concerned audio problems for fellow musicians, or IT-related issues for friends and extended family. At high school I had my first interactions with basic html \u0026amp; css web design and C# programming.\u003c/p\u003e\u003cp\u003eShortly after high school, I landed a job as an audio/video technician \u0026amp; project manager for conferences and live events. This allowed me to delve deeper into the art of solving tech-related problems for clients for a living - oh the joy! This meant putting together my optimal mix of tech and people to help clients achieve their goals in the best possible way. I spent five years in this profession before feeling the urge to branch out and take a new career path. Two years later, and with a diploma in complex IT sales, I find myself a part of the magnificent tretton37 family of driven problem solvers and skilled software developing craftsmen.\u003c/p\u003e\u003cp\u003eWhile I might not have pursued my early impressions of web and software development, I’ve focused instead on finding synergies between people, business and technology. Today, tretton37 presents me with the opportunity to continue on my tech-related problem solving path. This involves matching clients’ complex problems with the best team possible for creating well thought out, handcrafted solutions - always with world class user experiences and a proven business value top of mind.\u003c/p\u003e\u003cp\u003eMy spare time these days still involves playing and writing music, by myself or with my band. I also enjoy travelling with my girlfriend or going out for a nice dinner with friends. Oh - and did I mention that I still love playing computer and video games? :)\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": null,
        "linkedIn": "/in/gustav-hallberg/",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/gustav-hallberg-hektor-cv",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/gustav-hallberg-hektor-body",
        "imageWallOfLeetUrl": null,
        "highlighted": false
    }, {
        "name": "Paulina Raymond",
        "email": "cnhyvan.enlzbaq@gerggba82.pbz",
        "phoneNumber": "+07628861219",
        "office": "Stockholm",
        "tagLine": null,
        "mainText": "\u003cp\u003eAs a person I’m super curious and very outgoing, my biggest passion in life is to learn new things and to solve problems, that’s what’s really driving me forward.\u003c/p\u003e\u003cp\u003eWhen I’m not working, I love to travel and explore new beautiful places together with my family. I’m a big fan of flowers and plants, so I spend a lot of time in the garden. Because of my curiosity in life, I tend to add new hobbies quite often. My latest one was to become a gelato master and learn how to make real gelato because I’m crazy about ice cream.\u003c/p\u003e ",
        "gitHub": null,
        "twitter": null,
        "stackOverflow": null,
        "linkedIn": "/in/paulina-raymond/",
        "imagePortraitUrl": "https://tretton37img.blob.core.windows.net/ninja-portrait/paulina-raymond-cv1",
        "imageBodyUrl": "https://tretton37img.blob.core.windows.net/ninja-body/paulina-raymond-body",
        "imageWallOfLeetUrl": "https://tretton37img.blob.core.windows.net/ninja-body/paulina-raymond-leet",
        "highlighted": true
    },
];

const handlers = [
    rest.get('https://api.tretton37.com/ninjas', async (req, res, ctx) => {
      return res(ctx.json(fetchedProfiles))
    }),
];
const server = setupServer(...handlers)
let _localstorage;
describe("CardsList", () => {
    beforeAll(() => {
        server.listen();
        _localstorage = window.localStorage;
    });
    beforeEach(() => {
        window.localStorage = new LocalStorageMock();
    });
    it("Snapshot - Normal", async () => {
        const {container, getByText} = render(
            <FiltersProvider>
                <CardsList />
            </FiltersProvider>
        );
        expect(container).toMatchSnapshot();
        await wait(() => {
            expect(getByText("Stefan Viberg")).toBeTruthy();
        });
        expect(container).toMatchSnapshot();
    });
    it("Snapshot - Sorted", async () => {
        window.localStorage.setItem("sort", JSON.stringify("name-descending"));
        const {container, getByText} = render(
            <FiltersProvider>
                <CardsList />
            </FiltersProvider>
        );
        expect(container).toMatchSnapshot();
        await wait(() => {
            expect(getByText("Stefan Viberg")).toBeTruthy();
        });
        expect(container).toMatchSnapshot();
    });
    it("Snapshot - Text Filtered", async () => {
        window.localStorage.setItem("textFilter", JSON.stringify("ste"));
        const {container, getByText} = render(
            <FiltersProvider>
                <CardsList />
            </FiltersProvider>
        );
        expect(container).toMatchSnapshot();
        await wait(() => {
            expect(getByText("Stefan Viberg")).toBeTruthy();
        });
        expect(container).toMatchSnapshot();
    });
    it("Snapshot - Contact Filtered", async () => {
        window.localStorage.setItem("contactFilter", JSON.stringify("gitHub"));
        const {container, getByText} = render(
            <FiltersProvider>
                <CardsList />
            </FiltersProvider>
        );
        expect(container).toMatchSnapshot();
        await wait(() => {
            expect(getByText("Stefan Viberg")).toBeTruthy();
        });
        expect(container).toMatchSnapshot();
    });
    afterEach(() => {
        server.resetHandlers();
        cleanup;
    });
    afterAll(() => {
        server.close();
        window.localStorage = _localstorage;
    });
});