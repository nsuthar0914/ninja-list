import React from "react";
import useLocalStorage from "../hooks/useLocalStorage";

export const FiltersContext = React.createContext();

/**
 * This is an overarching provider for all kinds of filters and sorting that might be needed in 
 * various places in our app. Although we made a single page application, we would like to maintain
 * separation of concerns by keeping filters and cards in separate containers. This provider allows
 * us to have a central hub for getting and settin those values, passing the value on to children
 * via context api.
 * @param {*} props 
 */
const FiltersProvider = (props) => {
    const [sort, setSort] = useLocalStorage("sort", "name-ascending");
    const [textFilter, setTextFilter] = useLocalStorage("textFilter", "");
    const [contactFilter, setContactFilter] = useLocalStorage("contactFilter", "");
    return (
        <FiltersContext.Provider
            value={{
                sort,
                setSort,
                textFilter,
                setTextFilter,
                contactFilter,
                setContactFilter,
            }}
        >
            {props.children}
        </FiltersContext.Provider>
    );
};

export default FiltersProvider;
