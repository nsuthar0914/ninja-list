import React, { useContext } from "react";
import {cleanup, render, wait, fireEvent} from "@testing-library/react";
import {LocalStorageMock} from "../hooks/useLocalStorage.test";
import FiltersProvider, { FiltersContext } from "./FiltersProvider";

const SomeComponent = () => {
    const {
        sort,
        setSort,
        textFilter,
        setTextFilter,
        contactFilter,
        setContactFilter,
    } = useContext(FiltersContext);
    return (
        <div>
            <div>Selected sort: {sort}</div>
            <button onClick={e => setSort("name-descending")}>Change Sort</button>
            <div>Selected text filter: {textFilter}</div>
            <button onClick={e => setTextFilter("name")}>Change text filter</button>
            <div>Selected contact filter: {contactFilter}</div>
            <button onClick={e => setContactFilter("gitHub")}>Change contact filter</button>
        </div>
    );
};

let _localstorage;
describe("FiltersProvider", () => {
    beforeAll(() => {
        _localstorage = window.localStorage;
    });

    beforeEach(() => {
        window.localStorage = new LocalStorageMock();
    });

    afterAll(() => {
        window.localStorage = _localstorage;
    });

    afterEach(cleanup);

    it("Initialstate and update", async () => {
        const {getByText, debug} = render(<FiltersProvider>
            <SomeComponent />
        </FiltersProvider>);
        expect(getByText("Selected sort:")).toBeTruthy();
        expect(getByText("Selected text filter:")).toBeTruthy();
        expect(getByText("Selected contact filter:")).toBeTruthy();
        fireEvent.click(getByText("Change Sort"));
        await wait(() => {
            expect(getByText("Selected sort: name-descending")).toBeTruthy();
        });
        fireEvent.click(getByText("Change text filter"));
        await wait(() => {
            expect(getByText("Selected text filter: name")).toBeTruthy();
        });
        fireEvent.click(getByText("Change contact filter"));
        await wait(() => {
            expect(getByText("Selected contact filter: gitHub")).toBeTruthy();
        });
    });
});