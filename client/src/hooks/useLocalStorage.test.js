import {cleanup, renderHook} from "@testing-library/react-hooks";
import useLocalStorage from "./useLocalStorage";

export class LocalStorageMock {
    constructor() {
        this.store = {};
    }

    clear() {
        this.store = {};
    }

    getItem(key) {
        return this.store[key];
    }

    setItem(key, value) {
        if (key && value) this.store[key] = value.toString();
    }

    removeItem(key) {
        if (key) this.store[key] = null;
    }
}

let _localstorage;
describe("useLocalStorage", () => {
    beforeAll(() => {
        _localstorage = window.localStorage;
    });

    beforeEach(() => {
        window.localStorage = new LocalStorageMock();
    });

    afterAll(() => {
        window.localStorage = _localstorage;
    });

    afterEach(cleanup);

    it("initialState", () => {
        const {result} = renderHook(() => useLocalStorage("sort", "name-ascending"));
        expect(result.current[0]).toBe("name-ascending");
    });
    it("prefer localStorage", () => {
        window.localStorage.setItem("sort", '"name-descending"');
        const {result} = renderHook(() => useLocalStorage("sort", "name-ascending"));
        expect(result.current[0]).toBe("name-descending");
    });
    it("update", () => {
        const {result} = renderHook(() => useLocalStorage("sort", "name-ascending"));
        result.current[1]("name-descending");
        expect(result.current[0]).toBe("name-descending");
    });
    it("remove", () => {
        const {result} = renderHook(() => useLocalStorage("sort", "name-ascending"));
        result.current[1](null);
        expect(result.current[0]).toBe(null);
    });
});