import { useState } from "react";

/**
 * This is a custom hook for setting up localstorage usage in our app.
 * It is currently only used to store filters and sorting in local storage, to allow
 * persistence across refresh. Could be much more usedful later for authentication as well.
 * @param {string} key - key to be used in local storage 
 * @param {*} initialValue - value to be stringifies and stored
 */
const useLocalStorage = (key, initialValue) => {
    const [storedValue, setStoredValue] = useState(() => {
      try {
        const item = window.localStorage.getItem(key);
        return item ? JSON.parse(item) : initialValue;
      } catch (error) {
        console.log(error);
        return initialValue;
      }
    });
    const setValue = value => {
      try {
        setStoredValue(value);
        window.localStorage.setItem(key, JSON.stringify(value));
      } catch (error) {
        console.log(error);
      }
    };
    return [storedValue, setValue];
}

export default useLocalStorage;
